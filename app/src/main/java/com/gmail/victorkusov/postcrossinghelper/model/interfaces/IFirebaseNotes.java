package com.gmail.victorkusov.postcrossinghelper.model.interfaces;


public interface IFirebaseNotes {
    public void addNoteToFirebase();
    public void deleteNoteFromFirebase();
}
